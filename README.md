# Hello World in C

This is a project that shows the source code of a Hello World program in C.

It also offers a Makefile in order to obtain the program and
install it or uninstall it from a system. There are also other
targets to perform other actions.

- [Just install and execute](#just-install-and-execute)
- [Get the executable (program)](#getting-the-executable-program)
- [Installing the program](#installing-the-program)
- [Uninstalling the program](#uninstalling-the-program)
- [Other Makefile targets](#other-makefile-targets)
- [Credits](#credits)

## Just install and execute

Install by executing:

```sh
make install
```

Execute the program:

```sh
/usr/local/bin/helloworld
```

## Get the executable (program)

Together with the source file of the code there is also a Makefile (file)
that helps to automatize the following steps:

* Pre-proccess
* Compile
* Assemble
* Link

The previous four steps can be permormed by executing:

```sh
make
```

The result of executing the command of above is an exectuable file
(the final program) that then may be used by a (regular) user.

## Installing the program

The program (the executable file: helloworld) can be then installed
(by default at: /usr/local/bin) by executing:

```sh
make install
```

The default installation location can be configured by changing
the values of the enviroment variables PREFIX and DESTDIR, for example:

```sh
make PREFIX=/usr/bin DESTDIR=/tmp install
```

That will install the program at /tmp/usr/bin/ and then the program
can be executed as /tmp/usr/bin/helloworld.

## Uninstalling the program

The Makefile (file) not only has a target for install but also
for uninstalling the program. This can be done by executing:

```sh
make uninstall
```

If the enviroment variables PREFIX and DESTDIR were used when
`make install` was used then the same values need to be used when
executing `make uninstall` so that the program can be located
correctly and uninstalled from the system.

## Other Makefile targets

There also are more targets in the Makefile; one may want to take
a look at them. For example, there are targets to get the
executable file by doing pre-processing, compilling, assembling and
linking step by step.

## Credits

Programmer [Author]: CodeNoSchool

- [CodeNoSchool on YouTube](https://youtube.com/c/codenoschool)
- [CodeNoSchool on GitHub](https://gitlab.com/codenoschool)
- [CodeNoSchool on GitLab](https://gitlab.com/icodenoschool)
- [CodeNoSchool on Twitter](https://twitter.com/codenoschool)

