SHELL = /bin/sh

PREFIX = /usr/local

.SUFFIXES:
.SUFFIXES: .c .i .s .o

%.i : %.c
	cc -E $< -o $@

%.s : %.i
	cc -S $< -o $@

%.o : %.s
	as $< -o $@

all: helloworld

helloworld : main.o main.i main.s
	cc -o $@ $<

clean :
	rm -v helloworld

install : all
	mkdir -vp $(DESTDIR)$(PREFIX)/bin
	cp -vf helloworld $(DESTDIR)$(PREFIX)/bin/
	chmod -v 0755 $(DESTDIR)$(PREFIX)/bin/helloworld

uninstall :
	rm -v $(DESTDIR)$(PREFIX)/bin/helloworld

.PHONY: all clean install uninstall

